export const IMAGE_LOGIN = require('./images/image_login.jpg');

export const ICON_LOGO = require('./icons/icon_logo.png'),
ICON_ACCOUNT = require('./icons/icon_account.png'),
ICON_PASSWORD = require('./icons/icon_password.png'),
ICON_FACEBOOK = require('./icons/icon_facebook.png'),
ICON_GOOGLE = require('./icons/icon_google.png'),
ICON_HOME = require('./icons/icon_home.png'),
ICON_SPA = require('./icons/icon_spa.png'),
ICON_MESSENGER = require('./icons/icon_messenger.png'),
ICON_NOTIFICATION = require('./icons/icon_notification.png'),
ICON_PROFILE = require('./icons/icon_profile.png'),
ICON_MENU = require('./icons/icon_menu.png'),
ICON_SEARCH = require('./icons/icon_search.png');

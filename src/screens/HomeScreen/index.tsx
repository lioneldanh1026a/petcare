import React from 'react';
import { View, Text, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import { ICON_SEARCH } from '../../assets';
import styles from './style';

const HomeScreen = () => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <View style={styles.textHeader}>
                    <Text style={styles.textLogo}>PetCare</Text>
                </View>
                <TouchableOpacity style={styles.buttonSearch}>
                    <Image source={ICON_SEARCH} style={styles.imageHeader} />
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
};

export default HomeScreen;
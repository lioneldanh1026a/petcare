import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('screen');

export default StyleSheet.create({
    container: {
        flex: 1
    },
    viewLogin: {
        flex: 1,
        backgroundColor: '#ff920060'
    },
    viewIconLogin: {
        width: width,
        height: height * 0.4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconLogin: {
        width: '60%',
        height: '50%'
    },
    viewContentLogin: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewInput: {
        width: width * 0.8,
        marginBottom: 20,
        flexDirection: 'row',
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#fff',
    },
    iconInput: {
        tintColor: '#fff',
        width: 30,
        height: 30,
        marginBottom: 10
    },
    customInput: {
        paddingLeft: 10,
        color: '#fff'
    },
    viewButton: {
        width: width,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    customButton: {
        marginBottom: 20,
        width: width * 0.8,
        // color: '#ff9200',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        borderRadius: height / 2,
    },
    textButton: {
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    },
    buttonLogin: {
        backgroundColor: '#1bcd5a',
    },
    buttonFacebook: {
        backgroundColor: '#3b5998',
        flexDirection: 'row'
    },
    iconFacebook: {
        width: 25,
        height: 25,
        tintColor: '#fff'
    },
    iconGoogle: {
        width: 25,
        height: 25,
        backgroundColor: '#fff',
        marginRight: 5
    },
    buttonGoogle: {
        backgroundColor: '#4285f4',
        flexDirection: 'row',
    },
    viewOr: {
        flexDirection: 'row'
    },
    borderOr: {
        width: width*0.35,
        borderBottomWidth: 1,
        borderBottomColor: '#fff',
    },
    textOr: {
        color: '#fff',
        paddingLeft: 5,
        paddingRight: 5
    },
    buttonRegister: {
        backgroundColor: '#fff',
    },
    textButtonRegister: {
        color: '#ff9200',
        fontSize: 20,
        fontWeight: 'bold'
    },
    viewButtonRegister: {
        paddingTop: 20
    },
    forgotPassword: {
        color: '#fff'
    },
    buttonForgotPassword: {
        width: width * 0.8,
        marginBottom: 20,
        flexDirection: 'row',
        paddingHorizontal: 10,
    }
});

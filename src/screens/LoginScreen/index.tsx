import React from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { IMAGE_LOGIN, ICON_LOGO, ICON_ACCOUNT, ICON_PASSWORD, ICON_FACEBOOK, ICON_GOOGLE } from '../../assets';
import styles from './style';

interface Props {
    navigation: any;
}

const LoginScreen = (props : Props) => {

    const {navigation} = props;

    const _login = () => {
        navigation.navigate('TabStack');
    };

    return (
        <View style={styles.container}>
            <ImageBackground source={IMAGE_LOGIN} style={styles.container}>
                <View style={styles.viewLogin}>
                    <View style={styles.viewIconLogin}>
                        <Image source={ICON_LOGO} style={styles.iconLogin} resizeMode='contain' />
                    </View>
                    <View style={styles.viewContentLogin}>
                        <View style={styles.viewInput}>
                            <Image source={ICON_ACCOUNT} style={styles.iconInput} />
                            <TextInput
                                placeholder="Username"
                                placeholderTextColor='#fff'
                                style={styles.customInput}
                            />
                        </View>
                        <View style={styles.viewInput} >
                            <Image source={ICON_PASSWORD} style={styles.iconInput} />
                            <TextInput
                                placeholder="Password"
                                placeholderTextColor='#fff'
                                style={styles.customInput}
                            />
                        </View>
                        <View style={styles.buttonForgotPassword}>
                            <TouchableOpacity>
                                <Text style={styles.forgotPassword}>Forgot password?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewButton}>
                            <TouchableOpacity style={[styles.customButton, styles.buttonLogin]} onPress={_login}>
                                <Text style={styles.textButton}>Login</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewButton}>
                            <TouchableOpacity style={[styles.customButton, styles.buttonFacebook]}>
                                <Image source={ICON_FACEBOOK} style={styles.iconFacebook} />
                                <Text style={styles.textButton}>Login with Facebook</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.viewButton}>
                            <TouchableOpacity style={[styles.customButton, styles.buttonGoogle]}>
                                <Image source={ICON_GOOGLE} style={styles.iconGoogle} />
                                <Text style={styles.textButton}>Login with Google</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.viewButton, styles.viewOr]}>
                            <View style={styles.borderOr}></View>
                            <Text style={styles.textOr}>or</Text>
                            <View style={styles.borderOr}></View>
                        </View>
                        <View style={[styles.viewButton, styles.viewButtonRegister]}>
                            <TouchableOpacity style={[styles.customButton, styles.buttonRegister]}>
                                <Text style={styles.textButtonRegister}>Register</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ImageBackground>
        </View>
    );
};

export default LoginScreen;
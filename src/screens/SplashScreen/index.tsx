import React, { useEffect, useState } from 'react';
import { View, Text, SafeAreaView, Image, Animated, Easing } from 'react-native';
import { ICON_LOGO } from '../../assets';
import styles from './style';

const SplashScreen = () => {

    let animatedValue = new Animated.Value(0);

    const _handleAnimation = () => {
        Animated.timing(animatedValue, {
            toValue: 1,
            duration: 2000,
            easing: Easing.ease
        }).start()
    }

    useEffect(() => {
        _handleAnimation();
    })

    return (
        <SafeAreaView style={styles.container}>
            <Animated.Image
                source={ICON_LOGO}
                style={{
                    width: '50%',
                    height: '50%',
                    transform: [
                        {
                            translateX: animatedValue.interpolate({
                                inputRange: [0, 0.5],
                                outputRange: [0, 1.2]
                            })
                        },
                        {
                            translateY: animatedValue.interpolate({
                                inputRange: [0, 0.5],
                                outputRange: [0, 1.2]
                            })
                        },
                        {
                            scaleX: animatedValue.interpolate({
                                inputRange: [0, 0.5],
                                outputRange: [1, 1.2]
                            })
                        },
                        {
                            scaleY: animatedValue.interpolate({
                                inputRange: [0, 0.5],
                                outputRange: [1, 1.2]
                            })
                        }
                    ]
                }}
                resizeMode='contain' />
        </SafeAreaView>
    );
}

export default SplashScreen;
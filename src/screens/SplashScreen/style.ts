import { Dimensions, StyleSheet } from 'react-native';

const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: '#ff9200',
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: '50%',
        height: '50%',
    }
});
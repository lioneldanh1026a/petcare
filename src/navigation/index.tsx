import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { View, Text, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LoginScreen from '../screens/LoginScreen';
import SpaScreen from '../screens/SpaScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import MessengerScreen from '../screens/MessengerScreen';
import ProfileScreen from '../screens/ProfileScreen';
import HomeScreen from '../screens/HomeScreen';
import MenuScreen from '../screens/MessengerScreen';
import SplashScreen from '../screens/SplashScreen';
import { useDispatch, useSelector } from 'react-redux';
import { splashAction } from '../redux/actions/splashAction';

import {
    ICON_HOME,
    ICON_MESSENGER,
    ICON_NOTIFICATION,
    ICON_PROFILE,
    ICON_SPA,
    ICON_MENU
}
    from '../assets';

const AppNavigation = () => {

    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => {
            dispatch(splashAction(false));
        }, 3000);
    }, []);

    const { splashReducer } = useSelector((state: any) => state.splashReducer);

    const Stack = createStackNavigator();

    const LoginStack = () => {
        return (
            <Stack.Navigator headerMode='none'>
                <Stack.Screen name='LoginScreen' component={LoginScreen} />
                <Stack.Screen name='TabStack' component={Tab} />
            </Stack.Navigator>
        );
    };

    const TabNavigator = createBottomTabNavigator();

    const Tab = () => {
        return (
            <TabNavigator.Navigator tabBarOptions={{ showLabel: false, inactiveTintColor: '#66676b', activeTintColor: '#ff9200' }} >
                <TabNavigator.Screen
                    name="HomeStack"
                    component={HomeScreen}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <Image source={ICON_HOME} style={{ tintColor: color, width: 30, height: 30 }} />
                        ),
                    }}
                />
                <TabNavigator.Screen
                    name="SpaStack"
                    component={SpaScreen}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <Image source={ICON_SPA} style={{ tintColor: color, width: 30, height: 30 }} />
                        ),
                    }}
                />
                <TabNavigator.Screen
                    name="MessengerStack"
                    component={MessengerScreen}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <Image source={ICON_MESSENGER} style={{ tintColor: color, width: 30, height: 30 }} />
                        ),
                    }}
                />
                <TabNavigator.Screen
                    name="NotificationStack"
                    component={NotificationsScreen}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <Image source={ICON_NOTIFICATION} style={{ tintColor: color, width: 30, height: 30 }} />
                        ),
                    }}
                />
                <TabNavigator.Screen
                    name="ProfileStack"
                    component={ProfileScreen}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <Image source={ICON_PROFILE} style={{ tintColor: color, width: 30, height: 30 }} />
                        ),
                    }}
                />

                <TabNavigator.Screen
                    name="MenuStack"
                    component={MenuScreen}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <Image source={ICON_MENU} style={{ tintColor: color, width: 30, height: 30 }} />
                        ),
                    }}
                />
            </TabNavigator.Navigator>
        );
    };

    const Splash = createStackNavigator();

    const SplashStack = () => {
        return (
            <Splash.Navigator headerMode='none'>
                <Splash.Screen name='SplashScreen' component={SplashScreen} />
            </Splash.Navigator>
        );
    }

    return (
        <NavigationContainer>
            {
                splashReducer ?
                    <SplashStack />
                    :
                    <LoginStack />
            }
        </NavigationContainer>
    );
};

export default AppNavigation;
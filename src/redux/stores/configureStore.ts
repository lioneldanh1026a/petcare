import rootReducer from '../reducers/rootReducer';
import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import ReduxPersist from '../../configs/ReduxPersist';

const persistedReducer = persistReducer(ReduxPersist.storeConfig, rootReducer);
const store = createStore(
  persistedReducer
);
const persistor = persistStore(store);
export {store, persistor};

import Types from '../types';
const splashAction = (param: boolean) => ({
    type: Types.SET_SPLASH,
    param
});

export { splashAction }
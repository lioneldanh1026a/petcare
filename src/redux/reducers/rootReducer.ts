import {combineReducers} from 'redux';
import splashReducer from './splashReducer';

const rootReducer = combineReducers({
    splashReducer
});

export default rootReducer;

import Types from '../types';

const initialState = {
  splashReducer: true
};

const splashReducer = (state = initialState, action: any) => {
  const { type, param } = action;
  switch (type) {
    // Change og code
    case Types.SET_SPLASH: {
      return {
        // State
        ...state,
        // Redux Store
        splashReducer: param,
      }
    }

    // Default
    default: {
      return state;
    }
  }
};

export default splashReducer;

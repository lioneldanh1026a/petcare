import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20
    },
    textLogo: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#ff9200'
    },
    imageHeader: {
        width: 25,
        height: 25
    },
    buttonSearch: {
        width: 40,
        height: 40,
        backgroundColor: '#dedede',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35/2
    },
    textHeader: {
        alignItems: 'center',
        justifyContent: 'center',
    }
});
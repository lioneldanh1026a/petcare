import React from 'react';
import AppNavigation from './src/navigation';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Remote debugger']);
import { Provider } from 'react-redux';
import { store, persistor } from './src/redux/stores/configureStore';
import { PersistGate } from 'redux-persist/integration/react';

const App = () => {
  return (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppNavigation/>
        </PersistGate>
      </Provider>
  );
};
export default App;
